/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.menumaker;

import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.scene.control.TableView;

/**
 *
 * @author Jan.Lhotka
 */
public class Potravina_DB {

    private String id;
    private String nazev;
    private String zdrave;
    private String energie;
    private String tuky;
    private String bilkoviny;
    private String sacharidy;
    private String vaha;
    private String jidlo;
    private String menu;
    private String pc_energie;
    private String pc_tuky;
    private String pc_bilkoviny;
    private String pc_sacharidy;
    private String vlastni;    

    

    Potravina_DB(ResultSet rs, TableView table) throws SQLException {
        
        if (table.getId().equals("tab_menu")) {
            
            this.menu = rs.getString(DB.SL.MENU.toString());
            this.energie = rs.getString(DB.SL.ENERGIE.toString());
            this.tuky = rs.getString(DB.SL.TUKY.toString());
            this.bilkoviny = rs.getString(DB.SL.BILKOVINY.toString());
            this.sacharidy = rs.getString(DB.SL.SACHARIDY.toString());
            this.pc_energie = rs.getString(DB.SL.PC_ENERGIE.toString());
            this.pc_tuky = rs.getString(DB.SL.PC_TUKY.toString());
            this.pc_bilkoviny = rs.getString(DB.SL.PC_BILKOVINY.toString());
            this.pc_sacharidy = rs.getString(DB.SL.PC_SACHARIDY.toString());
            
        } else {

            this.nazev = rs.getString(DB.SL.NAZEV.toString());
            this.energie = rs.getString(DB.SL.ENERGIE.toString());
            this.id = rs.getString(DB.SL.ID.toString());
            this.zdrave = rs.getString(DB.SL.ZDRAVE.toString());
            this.tuky = rs.getString(DB.SL.TUKY.toString());
            this.bilkoviny = rs.getString(DB.SL.BILKOVINY.toString());
            this.sacharidy = rs.getString(DB.SL.SACHARIDY.toString());
            this.vlastni = rs.getString(DB.SL.VLASTNI.toString());

            if (!table.getId().equals("tab_potraviny")) {
                this.vaha = rs.getString(DB.SL.VAHA.toString());
                this.jidlo = rs.getString(DB.SL.JIDLO.toString());
                this.menu = rs.getString(DB.SL.MENU.toString());
            }
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String i) {
        this.id = i;
    }
    
    public String getNazev() {
        return nazev;
    }

    public void setNazev(String n) {
        this.nazev = n;
    }

    public String getZdrave() {
        return zdrave;
    }

    public void setZdrave(String z) {
        this.zdrave = z;
    }

    public String getEnergie() {
        return Parser.formatInt(energie);
    }

    public void setEnergie(String e) {
        this.energie = e;
    }
    
    public String getTuky() {
        return Parser.formatInt(tuky);
    }

    public void setTuky(String t) {
        this.tuky = t;
    }
    
    public String getBilkoviny() {
        return Parser.formatInt(bilkoviny);
    }

    public void setBilkoviny(String b) {
        this.bilkoviny = b;
    }

    public String getSacharidy() {
        return Parser.formatInt(sacharidy);
    }

    public void setSacharidy(String s) {
        this.sacharidy = s;
    }

    public String getVaha() {
        return Parser.formatInt(vaha);
    }

    public void setVaha(String v) {
        this.vaha = v;
    }
    
    public String getJidlo() {
        return jidlo;
    }

    public void setJidlo(String j) {
        this.jidlo = j;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String m) {
        this.menu = m;
    }

    public String getPc_energie() {
                
        return Parser.formatPercent(pc_energie);

    }

    public void setPc_energie(String pc_energie) {
        this.pc_energie = pc_energie;
    }

    public String getPc_tuky() {
        return Parser.formatPercent(pc_tuky);
    }

    public void setPc_tuky(String pc_tuky) {
        this.pc_tuky = pc_tuky;
    }

    public String getPc_bilkoviny() {
        return Parser.formatPercent(pc_bilkoviny);
    }

    public void setPc_bilkoviny(String pc_bilkoviny) {
        this.pc_bilkoviny = pc_bilkoviny;
    }

    public String getPc_sacharidy() {
        return Parser.formatPercent(pc_sacharidy);
    }

    public void setPc_sacharidy(String pc_sacharidy) {
        this.pc_sacharidy = pc_sacharidy;
    }

    public String getVlastni() {
        return vlastni;
    }

    public void setVlastni(String vlastni) {
        this.vlastni = vlastni;
    }
    
    
}
