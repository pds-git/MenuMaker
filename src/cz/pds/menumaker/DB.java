/*
 * Copyright (C) 2018 PDS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.pds.menumaker;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import cz.pds.menumaker.FXMLDocumentController.TABLES;
import org.apache.logging.log4j.*;



/**
 *
 * @author honya
 */
public final class DB {
    
    private static final Logger L = LogManager.getLogger(DB.class.getName());
    
    // Single DB
    private static final DB db = new DB();
    
    /* Název a cesta k databázi */
    private static final String DB_NAME = "jdbc:sqlite:data.db";
    
    /* Tabulky v DB */
    public static final String TAB_POTRAVINY = "potraviny";
    public static final String TAB_MENU = "menu";
    public static final String TAB_CILE = "cile";

    
    // Sloupce (zároveň řídí tabulku zobrazení)
    // název v DB, Zobrazovaný název, editovatelné, obravené
    enum SL {
        ID("id", "Id", false, false),
        NAZEV("nazev", "Název", false, false),
        ZDRAVE("zdrave", "Zdravé", false, false),
        ENERGIE("energie", "Energie", false, false),
        TUKY("tuky", "Tuky", false, false),
        BILKOVINY("bilkoviny", "Bílkoviny", false, true),
        SACHARIDY("sacharidy", "Sacharidy", false, false),
        VAHA("vaha", "Váha", true, false),
        JIDLO("jidlo", "Jídlo", false, false),
        MENU("menu", "Menu", false, false),
        PC_ENERGIE("pc_energie", "% energie", false, true),
        PC_TUKY("pc_tuky", "% tuky", false, true),
        PC_BILKOVINY("pc_bilkoviny", "% bílkoviny", false, true),
        PC_SACHARIDY("pc_sacharidy", "% sacharidy", false, true),
        TOLERANCE("tolerance", "Tolerance", false, false),
        VLASTNI("vlastni", "Vlastní", false, false);
        
        private String name, label;
        private boolean editable, colored;
        
        SL(String n, String l, boolean e, boolean c)
        { name = n; label = l; editable = e; colored = c;}

        @Override
        public String toString() { return name; }
        String getLabel() { return label; }
        boolean getEditable() { return editable; }
        boolean getColored() { return colored; }
    }
    
    // Sigleton
    public static DB getDB() {
        return db;
    }
    
    public DB() {
        // Existuje knihovna s sqlite?
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            L.fatal("DB lib not found:", e);
            System.exit(1);
        }
        
        // Create DB
        String sql = "CREATE TABLE IF NOT EXISTS " + TAB_POTRAVINY
                + "(" + SL.ID + "   INTEGER PRIMARY KEY     AUTOINCREMENT,"
                + SL.NAZEV + "      TEXT        NOT NULL, "
                + SL.ZDRAVE + "     TEXT        , "
                + SL.VLASTNI + "    INTEGER     NOT NULL, "
                + SL.ENERGIE + "    REAL        NOT NULL, "
                + SL.TUKY + "       REAL        NOT NULL, "
                + SL.BILKOVINY + "  REAL        NOT NULL, "
                + SL.SACHARIDY + "  REAL        NOT NULL)";

        update(sql);

        sql = "CREATE TABLE IF NOT EXISTS " + TAB_MENU
                + "(" + SL.ID + "   INTEGER PRIMARY KEY     AUTOINCREMENT,"
                + SL.VAHA + "       REAL        , "
                + SL.JIDLO + "      TEXT        , "
                + SL.MENU + "       TEXT        ,"
                + SL.NAZEV + "      TEXT        NOT NULL, "
                + SL.ZDRAVE + "     TEXT        , "
                + SL.VLASTNI + "    INTEGER     NOT NULL, "
                + SL.ENERGIE + "    REAL        NOT NULL, "
                + SL.TUKY + "       REAL        NOT NULL, "
                + SL.BILKOVINY + "  REAL        NOT NULL, "
                + SL.SACHARIDY + "  REAL        NOT NULL);";
        
        update(sql);
        
                sql = "CREATE TABLE IF NOT EXISTS " + TAB_CILE
                + "("
                + SL.ENERGIE + "    REAL        , "
                + SL.TUKY + "       REAL        , "
                + SL.BILKOVINY + "  REAL        , "
                + SL.PC_BILKOVINY + "  REAL        , "
                + SL.SACHARIDY + "  REAL        , "
                + SL.TOLERANCE + "  REAL        ,"
                + FXMLDocumentController.TABLES.tab_menu_snidane + "  REAL,"
                + FXMLDocumentController.TABLES.tab_menu_dopol_svacina + "  REAL,"
                + FXMLDocumentController.TABLES.tab_menu_obed + "  REAL,"
                + FXMLDocumentController.TABLES.tab_menu_odpol_svacina + "  REAL,"
                + FXMLDocumentController.TABLES.tab_menu_vecere + "  REAL);";
        
        update(sql);

    }

    /* http://www.sqlitetutorial.net/sqlite-java/select/ */
    // Vrací připojení k databázi
    private Connection connect() {

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_NAME);
        } catch (SQLException e) {
            L.error("DB connect error: " + DB_NAME, e);

        }

        return conn;
    }

    // Příkazy, které nic nevrací (INSERT, DELETE, UPDATE, CREATE..)
    private void update(String sql) {

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            L.debug("SQL UPDATE: " + sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            L.error("SQL update err: " + sql, e);
        }
    }
    
    // TODO - insert bez uzavírání
    public void insert(String table, Map<String, Object> m) {

        // Switch by table
        String sql = "INSERT INTO " + table
                + " (" + SL.NAZEV + ", " + SL.ENERGIE
                + ", " + SL.TUKY + ", " + SL.BILKOVINY + ", " + SL.SACHARIDY
                + ", " + SL.ZDRAVE + ", " + SL.VLASTNI + ")"
                + " VALUES ('" + m.get(SL.NAZEV.toString())
                + "', " + m.get(SL.ENERGIE.toString()) + ", " + m.get(SL.TUKY.toString())
                + ", " + m.get(SL.BILKOVINY.toString()) + ", " + m.get(SL.SACHARIDY.toString())
                + ", '" + m.get(SL.ZDRAVE.toString()) + "', " + m.get(SL.VLASTNI.toString()) +" );";

        update(sql);

    }
    
    public void insertTarget(String table, Map<String, Object> m) {

        // Switch by table
        String sql = "INSERT INTO " + table
                + " (" + SL.ENERGIE
                + ", " + SL.TUKY + ", " + SL.BILKOVINY + ", " + SL.PC_BILKOVINY + ", " + SL.SACHARIDY + ", " + SL.TOLERANCE + ", " 
                + TABLES.tab_menu_snidane + ", " + TABLES.tab_menu_dopol_svacina +  ", "
                + TABLES.tab_menu_obed + ", " + TABLES.tab_menu_odpol_svacina +  ", "
                + TABLES.tab_menu_vecere + ")"
                + " VALUES ((" + m.get(TABLES.tab_menu_snidane.toString()) + " + " + m.get(TABLES.tab_menu_dopol_svacina.toString()) + " + "
                + m.get(TABLES.tab_menu_obed.toString()) + " + " + m.get(TABLES.tab_menu_odpol_svacina.toString()) + " + " + m.get(TABLES.tab_menu_vecere.toString()) + "), "
                + m.get(SL.TUKY.toString()) + ", " + m.get(SL.BILKOVINY.toString()) + ", " + m.get(SL.PC_BILKOVINY.toString()) + ", " + m.get(SL.SACHARIDY.toString()) + ", "
                + m.get(SL.TOLERANCE.toString()) + ", " + m.get(TABLES.tab_menu_snidane.toString()) + ", "
                + m.get(TABLES.tab_menu_dopol_svacina.toString()) + ", " + m.get(TABLES.tab_menu_obed.toString()) + ", "
                + m.get(TABLES.tab_menu_odpol_svacina.toString()) + ", " + m.get(TABLES.tab_menu_vecere.toString()) + ");";

        update(sql);

    }
    
    
    public void fillTable(TableView tab_menu, TableView table, Label[] l) {

        String db_tab;
        String query = "";
        
        switch (table.getId()) {
            case "tab_potraviny":
                db_tab = TAB_POTRAVINY;
                // Profiltrování - postupně se skládá query
                
                query = "SELECT * FROM " + db_tab;

                if (FXMLDocumentController.hledane.length() > 2) {
                    query = query + " WHERE " + DB.SL.NAZEV + " LIKE '%" + FXMLDocumentController.hledane + "%' AND";
                } else {
                    query = query + " WHERE 1 = 1 AND";
                }

                if (FXMLDocumentController.aktualni_zdrave) {
                    query = query + " IFNULL(" + SL.ZDRAVE + ",'') = 'Z' AND";
                } else {
                    query = query + " 1 = 1 AND";
                }

                if (FXMLDocumentController.aktualni_vlastni) {
                    query = query + " " + SL.VLASTNI + " = 1";
                } else {
                    query = query + " 1 = 1";
                }

                query = query + ";";

                break;
            case "tab_menu_snidane":
                db_tab = TAB_MENU;
                query = "SELECT * FROM " + db_tab
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu +"'"
                        + " AND " + SL.JIDLO + " = '" + table.getId() + "';";
                break;
            case "tab_menu_dopol_svacina":
                db_tab = TAB_MENU;
                query = "SELECT * FROM " + db_tab
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu +"'"
                        + " AND " + SL.JIDLO + " = '" + table.getId() + "';";
                break;
            case "tab_menu_obed":
                db_tab = TAB_MENU;
                query = "SELECT * FROM " + db_tab
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu +"'"
                        + " AND " + SL.JIDLO + " = '" + table.getId() + "';";
                break;
            case "tab_menu_odpol_svacina":
                db_tab = TAB_MENU;
                query = "SELECT * FROM " + db_tab
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu +"'"
                        + " AND " + SL.JIDLO + " = '" + table.getId() + "';";
                break;
            case "tab_menu_vecere":
                db_tab = TAB_MENU;
                query = "SELECT * FROM " + db_tab
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu +"'"
                        + " AND " + SL.JIDLO + " = '" + table.getId() + "';";
                break;
            case "tab_menu":
                db_tab = TAB_MENU;
                query = "SELECT " + SL.MENU + ", SUM(" + SL.ENERGIE + " / 100 * " + SL.VAHA + ") AS " + SL.ENERGIE
                        + " ,SUM(" + SL.TUKY + " / 100 * " + SL.VAHA + ") AS " + SL.TUKY
                        + " ,SUM(" + SL.BILKOVINY + " / 100 * " + SL.VAHA + ") AS " + SL.BILKOVINY
                        + " ,SUM(" + SL.SACHARIDY + " / 100 * " + SL.VAHA + ") AS " + SL.SACHARIDY
                        + " ,SUM(" + SL.ENERGIE + " * " + SL.VAHA + ") / (SELECT " + SL.ENERGIE + " FROM " + TAB_CILE + ") AS " + SL.PC_ENERGIE
                        + " ,SUM(" + SL.TUKY + " / 100 * " + SL.VAHA + ") * 39 / SUM(" + SL.ENERGIE + " / 100 * " + SL.VAHA + " * 4.18) * 100 AS " + SL.PC_TUKY
                        + " ,SUM(" + SL.BILKOVINY + " / 100 * " + SL.VAHA + ") * 17 / SUM(" + SL.ENERGIE + " / 100 * " + SL.VAHA + " * 4.18) * 100 AS " + SL.PC_BILKOVINY
                        + " ,SUM(" + SL.SACHARIDY + " / 100 * " + SL.VAHA + ") * 17 / SUM(" + SL.ENERGIE + " / 100 * " + SL.VAHA + " * 4.18) * 100 AS " + SL.PC_SACHARIDY
                        + " FROM " + db_tab
                        + " GROUP BY " + SL.MENU + ";";
                break;
            default:
                L.info("Špatná tabulka: " + table.getId());
        }

        L.debug(query);
        
        fillTable(tab_menu, table, query, l);
        
    }
    
    public void fillTable(TableView tab_menu,TableView table, String query, Label[] l) {

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(query)) {

            if (table.getColumns().isEmpty()) {

                // Set column count (get number columns from query and set to table)
                int columns = rs.getMetaData().getColumnCount();

                TableColumn[] sloupce = new TableColumn[columns];

                for (int i = 0; i < columns; i++) {
                    for (SL sloup : SL.values()) {
                        // Když je jméno sloupce shodné s názvem sloupce v DB
                        if (sloup.toString().equals(rs.getMetaData().getColumnName(i + 1))) {

                            // Set Column header label
                            sloupce[i] = new TableColumn(sloup.getLabel());
                            // Set key column - data
                            sloupce[i].setCellValueFactory(new PropertyValueFactory<>(sloup.toString()));

                            // Pouze pro editovatelné sloupce
                            if (sloup.getEditable()) {
                                // On edit cell
                                sloupce[i].setCellFactory(TextFieldTableCell.<Potravina_DB>forTableColumn());
                                sloupce[i].setOnEditCommit(new EventHandler<CellEditEvent<Potravina_DB, String>>() {
                                    @Override
                                    public void handle(CellEditEvent<Potravina_DB, String> t) {

                                        String sql;
                                        try {
                                            sql = "UPDATE " + TAB_MENU
                                                    + " SET " + sloup + " = " + Parser.parseDouble(t.getNewValue())
                                                    + " WHERE " + SL.ID + " = " + t.getTableView().getItems().get(
                                                            t.getTablePosition().getRow()).getId() + ";";

                                            update(sql);

                                            // Update tabulek v četně menu
                                            fillTable(tab_menu, tab_menu, l);
                                            fillTable(tab_menu, table, l);
                                            // Update labelů s kcal
                                            fillEatsKcal(l);

                                        } catch (Exception e) {
                                            L.error("Chybně zadaná hodnota", e);
                                            fillTable(tab_menu, table, l);

                                        }

                                    }
                                }
                                );
                            }
                            
                            // Pouze pokud má být podbarvené a pouze pro tabulku s menu
                            if (sloup.getColored() && table.getId().equals("tab_menu")) {
                                sloupce[i].setCellFactory(column -> {
                                    TableCell<Potravina_DB, String> c = new TableCell<Potravina_DB, String>() {
                                        @Override
                                        protected void updateItem(String item, boolean empty) {
                                            super.updateItem(item, empty);

                                            setText(empty ? "" : getItem());
                                            setGraphic(null);
                                            
                                            // Hodnota z nastavení podle sloupce
                                            String p_nast_s;
                                            
                                            switch (sloup) {
                                                case PC_ENERGIE:
                                                    p_nast_s = queryValue("SELECT " + SL.TOLERANCE + " FROM " + TAB_CILE + ";");
                                                    break;
                                                case PC_TUKY:
                                                    p_nast_s = queryValue("SELECT " + SL.TUKY + " FROM " + TAB_CILE + ";");
                                                    break;
                                                case PC_BILKOVINY:
                                                    p_nast_s = queryValue("SELECT " + SL.BILKOVINY + " FROM " + TAB_CILE + ";");
                                                    break;
                                                case BILKOVINY:
                                                    p_nast_s = queryValue("SELECT " + SL.PC_BILKOVINY + " FROM " + TAB_CILE + ";");
                                                    break;
                                                case PC_SACHARIDY:
                                                    p_nast_s = queryValue("SELECT " + SL.SACHARIDY + " FROM " + TAB_CILE + ";");
                                                    break;
                                                default:
                                                    p_nast_s = "";
                                                    break;
                                            }
                                            
                                            
                                            double p_nast_d = 0.0;
                                            double p_akt_d = 0.0;
                                            
                                            try {
                                                p_nast_d = Parser.parseDouble(p_nast_s);
                                                p_akt_d = Parser.parseDouble(item);
                                            } catch (Exception e) {
                                                L.info("Chyba parsování procenta a nastavení! ", e);
                                            }

                                            // Logika podbarvení podle sloupce
                                            if (sloup == SL.PC_ENERGIE && !empty) {
                                                
                                                if (p_akt_d > 100 - p_nast_d && p_akt_d < 100 + p_nast_d) {
                                                    setStyle("-fx-background-color: #33cc33");
                                                } else {
                                                    setStyle("-fx-background-color: #e60000");
                                                }

                                            } else if ((sloup == SL.PC_TUKY || sloup == SL.PC_SACHARIDY ) && !empty) {
                                                
                                                if ( p_akt_d < p_nast_d) {
                                                    setStyle("-fx-background-color: #33cc33");
                                                } else {
                                                    setStyle("-fx-background-color: #e60000");
                                                }
                                                
                                                
                                            } else if (sloup == SL.PC_BILKOVINY && !empty) {
                                                
                                                if ( p_akt_d > p_nast_d) {
                                                    setStyle("-fx-background-color: #33cc33");
                                                } else {
                                                    setStyle("-fx-background-color: #e60000");
                                                }

                                            }  else if (sloup == SL.BILKOVINY && !empty) {
                                                
                                                if ( p_akt_d > p_nast_d) {
                                                    setStyle("-fx-background-color: #33cc33");
                                                } else {
                                                    setStyle("-fx-background-color: #e60000");
                                                }

                                            }
  
                                        }
                                    };
                                    
                                    return c;
                                });
  
                            }
                            

                            // Schovej nepotřebné sloupce
                            if (sloup.toString().equals(SL.ID.toString())
                                    || sloup.toString().equals(SL.JIDLO.toString())
                                    || sloup.toString().equals(SL.MENU.toString())) {
                                sloupce[i].setVisible(false);
                            }
                            if (sloup.toString().equals(SL.MENU.toString()) && table.getId().equals("tab_menu")) {
                                sloupce[i].setVisible(true);
                            }

                            // Minimální šířka u názvu
                            if (sloup.toString().equals(SL.NAZEV.toString())
                                    || sloup.toString().equals(SL.MENU.toString())) {

                                sloupce[i].setMinWidth(110);
                            }
                            

                            break;
                        }
                    }
                }
                // Add all cells
                table.getColumns().addAll((Object[]) sloupce);

            }
            
            // Table data variable
            ObservableList<Potravina_DB> data = FXCollections.observableArrayList();

            while (rs.next()) {
                        
                data.add(new Potravina_DB(rs, table));                
            }

            // Bind data to table
            table.setItems(data);

        } catch (SQLException e) {
            L.error("Fill table " + table.getId(), e);
        }

        table.setVisible(true);
    }
    
    
    public void fillEatsKcal(Label[] l) {

        double[] sums = new double[5];
        double[] targets = new double[5];
        double tolerance = 0;
        int i = 0;

        // Tolerance
        try {
            tolerance = Parser.parseDouble(queryValue("SELECT " + SL.TOLERANCE + " FROM " + TAB_CILE + ";"));
        } catch (Exception e) {
            L.info("Chyba parsování součtů jídel", e);
        }
        
        
        for (TABLES tab : TABLES.values()) {

            try {
                sums[i] = Parser.parseDouble(queryValue("SELECT SUM(" + SL.VAHA + " * " + SL.ENERGIE + " / 100)"
                        + " FROM " + TAB_MENU + ""
                        + " WHERE " + SL.MENU + " = '" + FXMLDocumentController.aktualni_menu + "' AND"
                        + " " + SL.JIDLO + " = '" + tab + "';"));
                
                targets[i] = Parser.parseDouble(queryValue(
                        "SELECT " + tab
                        + " FROM " + DB.TAB_CILE + ";"));

            } catch (Exception e) {
                L.info("Chyba parsování součtů jídel", e);
            }
            
            // Naplnění ukazatelů kalorií jednotlivých jídel
            l[i].setText((int) sums[i] + " / " + (int) targets[i] + " Kcal");

            double pc = sums[i] / targets[i] * 100;
            
            if (pc < (100 + tolerance) && pc > (100 - tolerance)) {
                l[i].setStyle("-fx-background-color: #33cc33; -fx-text-fill: black;");
                
            } else {
                
                l[i].setStyle("-fx-background-color: #e60000; -fx-text-fill: white;");
            }
            
            i++;
        }
 
    }
    
    public String queryValue(String query) {

        String out = "";
        
        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(query)) {
            
            out = rs.getString(1);
            
            
        } catch (SQLException e) {
            L.error("queryValue ERR", e.getMessage());
        }
        
        return out;
    }
    
    public ArrayList<Potravina_DB> queryValue(String query, TableView table) {

        ArrayList<Potravina_DB> out = new ArrayList<>();

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(query)) {

            while (rs.next()) {

                out.add(new Potravina_DB(rs, table));
            }

        } catch (SQLException e) {
            L.error("queryValue ERR", e.getMessage());
        }

        return out;
    }
    

    // Smaže vše ze zadané tabulky
    public void cleanTable(String table) {
        update("DELETE FROM " + table + ";");
    }
    
    public void delete(String tab, SL kde, String co) {

        String sql = "DELETE FROM " + tab
                + " WHERE " + kde + " = '" + co + "';";

        update(sql);
    }
    
    public void copyToTable(String tFrom, String tTo, String id) {

        String sql = "INSERT INTO " + tTo + " (" + SL.NAZEV + ", " + SL.ENERGIE
                + ", " + SL.TUKY + ", " + SL.BILKOVINY + ", " + SL.SACHARIDY
                + ", " + SL.ZDRAVE + ", " + SL.VLASTNI + ")"
                + " SELECT " + SL.NAZEV  + ", " + SL.ENERGIE
                + ", " + SL.TUKY + ", " + SL.BILKOVINY + ", " + SL.SACHARIDY
                + ", " + SL.ZDRAVE + ", " + SL.VLASTNI
                + " FROM " + tFrom
                + " WHERE ID = " + id + ";";

        update(sql);
    }
 
    public void updateParam(SL sloupec, String hodnota) {
        String sql = "UPDATE " + TAB_MENU
                + " SET " + sloupec + " = '" + hodnota + "'"
                + " WHERE " + sloupec + " IS NULL;";
        update(sql);
    }
    
    public void updateParam(SL sloupec, String hodnota, SL kde, String co) {
        String sql = "UPDATE " + TAB_MENU
                + " SET " + sloupec + " = '" + hodnota + "'"
                + " WHERE " + kde + " = '" + co + "';";
        update(sql);
    }
    
}
