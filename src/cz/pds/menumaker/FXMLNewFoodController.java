/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.menumaker;

import java.net.URL;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * FXML Controller class
 *
 * @author Jan.Lhotka
 */
public class FXMLNewFoodController implements Initializable {

    private static final Logger L = LogManager.getLogger(FXMLNewFoodController.class.getName());
    
    @FXML
    private TextField energie;
    @FXML
    private TextField tuky;
    @FXML
    private TextField bilkoviny;
    @FXML
    private TextField sacharidy;
    @FXML
    private TextField nazev;
    @FXML
    private CheckBox zdrave;
    @FXML
    private Button ok_button;
    
    DB db;
    
    @FXML
    private void handleOk(ActionEvent e) {
        
        
        // TODO - ověření dat
                
        Map<String, Object> m = new HashMap<>();

        m.put(DB.SL.NAZEV.toString(), nazev.getText());
        m.put(DB.SL.ZDRAVE.toString(), zdrave.isSelected() ? "Z" : "");
        m.put(DB.SL.VLASTNI.toString(), 1);

        try {
            m.put(DB.SL.ENERGIE.toString(), Parser.parseDouble(energie.getText()));
            m.put(DB.SL.TUKY.toString(), Parser.parseDouble(tuky.getText()));
            m.put(DB.SL.BILKOVINY.toString(), Parser.parseDouble(bilkoviny.getText()));
            m.put(DB.SL.SACHARIDY.toString(), Parser.parseDouble(sacharidy.getText()));

            db.insert(DB.TAB_POTRAVINY, m);

            // Které tlačítko jsem zmáčknul
            Button b = (Button) e.getSource();
            Stage stage = (Stage) b.getScene().getWindow();
            stage.close();

        } catch (ParseException ex) {
            L.error("Chyba parsování nového jídla", ex);
        }

    }
    
    
    @FXML
    private void handleStorno(ActionEvent e) {
        
        // Které tlačítko jsem zmáčknul
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.close();
        
    }
    
    // Check filled all paameters
    @FXML
    private void handleEdit(KeyEvent e) {

        if (!sacharidy.getText().isEmpty() && !nazev.getText().isEmpty()
                && !energie.getText().isEmpty() && !tuky.getText().isEmpty()
                && !bilkoviny.getText().isEmpty()) {

            ok_button.setDisable(false);

        } else {
            ok_button.setDisable(true);
        }

    }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        db = DB.getDB();
        
    }    
    
}
