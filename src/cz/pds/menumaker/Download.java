/*
 * Copyright (C) 2018 PDS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cz.pds.menumaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 *
 * @author honya
 */
public class Download {

    public void downHttpText(String url) throws MalformedURLException, IOException {

        URL u;
        HttpURLConnection urlcon = null;
        
        try {
            
            u = new URL(url);
            urlcon = (HttpURLConnection) u.openConnection();

            // String based reader
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(urlcon.getInputStream()));
                       
            Parser p = new Parser(in);
            p.parseCSV();

        } finally {
            urlcon.disconnect();
        }

    }

}
