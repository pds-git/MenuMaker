/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.menumaker;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 *
 * @author Jan.Lhotka
 */
public class FXMLDocumentController implements Initializable {
    
    private static final Logger L = LogManager.getLogger(FXMLDocumentController.class.getName());
    
    enum TABLES {
        tab_menu_snidane("Snídaně"),
        tab_menu_dopol_svacina("Dopolední svačina"),
        tab_menu_obed("Oběd"),
        tab_menu_odpol_svacina("Odpolední svačina"),
        tab_menu_vecere("Večeře");

        String label;
        TABLES(String l) { label = l; }
        String getLabel() { return label; }
    }
    
    @FXML
    private TableView tab_potraviny;
    @FXML
    private TableView tab_menu_snidane;
    @FXML
    private TableView tab_menu_dopol_svacina;
    @FXML
    private TableView tab_menu_obed;
    @FXML
    private TableView tab_menu_odpol_svacina;
    @FXML
    private TableView tab_menu_vecere;
    @FXML
    private TableView tab_menu;
    @FXML
    private Button aktualizuj;
    @FXML
    private TextField nazev_menu;
    @FXML
    private TextField search;
    @FXML
    private CheckBox zdrave;
    @FXML
    private CheckBox vlastni;
    @FXML
    private ProgressIndicator progres;
    @FXML
    private Label kalorie_snidane;
    @FXML
    private Label kalorie_dopol_svacina;
    @FXML
    private Label kalorie_obed;
    @FXML
    private Label kalorie_odpol_svacina;
    @FXML
    private Label kalorie_vecere;
    
    public static String aktualni_menu;
    public static boolean aktualni_zdrave, aktualni_vlastni;
    public static String hledane;

    DB db;
    
    
    @FXML
    private void handleAktualizujAction(ActionEvent event) {
        L.debug("Aktualizace potravin");
        
        Task task = new Task<Void>() {
        
            @Override
            protected Void call() {
                
                // Musí být nový objekt kvůli vláknu
                DB db = new DB();
                
                aktualizuj.setDisable(true);
                progres.setVisible(true);

                db.delete(DB.TAB_POTRAVINY, DB.SL.VLASTNI, "0");

                Download d = new Download();

                try {
                    d.downHttpText("https://honya.cz/food/list.csv");

                } catch (IOException ex) {
                    L.error("Download or parse error", ex);
                }

                aktualizujTabulky(db, tab_potraviny);
                
                aktualizuj.setDisable(false);
                progres.setVisible(false);
                
                return null;
            }
            
        };
        
        new Thread(task).start();

    }
    
    @FXML
    private void handleNazevMenu(ActionEvent event) {
        L.debug("Text pole: " + nazev_menu.getText());
        
        if (nazev_menu.getText().equals(aktualni_menu)) {
            
            db.updateParam(DB.SL.MENU, nazev_menu.getText());
            
        } else {
            
            db.updateParam(DB.SL.MENU, nazev_menu.getText(), DB.SL.MENU, aktualni_menu);
            aktualni_menu = nazev_menu.getText();
        }
        
        tab_potraviny.setDisable(false);
        aktualizujTabulky(db, tab_menu);
    }
    
    @FXML
    private void handleNewMenu(ActionEvent event) {
        aktualni_menu = "Zadejte název";
        nazev_menu.setText(aktualni_menu);
        nazev_menu.requestFocus();
        aktualizujTabulky(db, tab_menu, tab_menu_dopol_svacina, tab_menu_obed, tab_menu_odpol_svacina,
                tab_menu_snidane, tab_menu_vecere);
        tab_potraviny.setDisable(true);
    }
    
    @FXML
    private void handleChangeName(KeyEvent e) {
        
        if (!e.getCode().toString().equals("ENTER")) {
            tab_potraviny.setDisable(true);
        }
    }
 
    @FXML
    private void handleRemoveMenu(ActionEvent event) {

        if (!aktualni_menu.equals("")) {
            db.delete(DB.TAB_MENU, DB.SL.MENU, aktualni_menu);
            aktualizujTabulky(db, tab_menu, tab_menu_dopol_svacina, tab_menu_obed, tab_menu_odpol_svacina,
                tab_menu_snidane, tab_menu_vecere);

            aktualni_menu = "";
            nazev_menu.setText("");
            tab_potraviny.setDisable(true);
        }
    }
    
    
    // Pres delete key
    @FXML
    private void handleKeyPres(KeyEvent e) {

        TableView t = (TableView) e.getSource();

        if (e.getCode().toString().equals("DELETE")
                && !t.getSelectionModel().isEmpty()) {

            if (t.getId().equals("tab_potraviny")) {

                Potravina_DB p = (Potravina_DB) t.getSelectionModel().getSelectedItem();

                db.delete(DB.TAB_POTRAVINY, DB.SL.ID, p.getId());
                
                aktualizujTabulky(db, t);
                
                
            } else if (t.getId().equals("tab_menu")) {

                if (!aktualni_menu.equals("")) {
                    db.delete(DB.TAB_MENU, DB.SL.MENU, aktualni_menu);
                    aktualizujTabulky(db, tab_menu, tab_menu_dopol_svacina, tab_menu_obed, tab_menu_odpol_svacina,
                            tab_menu_snidane, tab_menu_vecere);

                    aktualni_menu = "";
                    nazev_menu.setText("");
                }

            } else {

                Potravina_DB p = (Potravina_DB) t.getSelectionModel().getSelectedItem();

                db.delete(DB.TAB_MENU, DB.SL.ID, p.getId());
                aktualizujTabulky(db, t, tab_menu);
            }

        }

    }
    
    // Click on menu
    @FXML
    private void handleClick(MouseEvent e) {
        
        L.debug("Click on menu: " + e.getButton());
        
        TableView t = (TableView) e.getSource();

        if (!t.getSelectionModel().isEmpty()) {
            
            // Get values from table to Potravina object
            Potravina_DB p = (Potravina_DB) t.getSelectionModel().getSelectedItem();
            
            // Kliknuto sekundárním -> zkopíruj meny jako HTML
            if (e.getButton().equals(MouseButton.SECONDARY)) {

                String sql = "SELECT * FROM " + DB.TAB_MENU +
                        " WHERE " + DB.SL.MENU + " = '" + p.getMenu() + "';";
                
                ArrayList<Potravina_DB> data = db.queryValue(sql, tab_menu_snidane);
                
                // Get menu from parser
                String[] s = Parser.menuToHtml(data);

                final ClipboardContent content = new ClipboardContent();
                content.putString(s[0]);
                content.putHtml(s[1]);
                Clipboard.getSystemClipboard().setContent(content);
                
            } else { // Jinak ukaž dané menu

                L.debug("Označené" + p.getMenu());

                aktualni_menu = p.getMenu();
                nazev_menu.setText(aktualni_menu);
                aktualizujTabulky(db, tab_menu_dopol_svacina, tab_menu_obed, tab_menu_odpol_svacina,
                        tab_menu_snidane, tab_menu_vecere);
                tab_potraviny.setDisable(false);
            }

        }
        
    }
    
    @FXML
    private void handleOpenSettings(ActionEvent event) {
        
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("FXMLMenu.fxml"));
        Scene scene;

        try {
            scene = new Scene(fxmlLoader.load(), 700, 300);

            Stage stage = new Stage();
            stage.setTitle("Nastavení cílů");
            stage.setScene(scene);

            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    aktualizujTabulky(db, tab_menu);
                }
            });

            stage.show();

        } catch (IOException ex) {
            L.error("Open settings error", ex);
        }

    }
    
    @FXML
    private void handleNewFood(ActionEvent event) {
        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("FXMLNewFood.fxml"));

            Scene scene = new Scene(fxmlLoader.load(), 460, 300);
            Stage stage = new Stage();
            stage.setTitle("Nová potravina");
            stage.setScene(scene);
            
            stage.setOnHidden(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    aktualizujTabulky(db, tab_potraviny);
                }
            });
            
            stage.show();
        } catch (IOException e) {
            
        }
    }
    
    
    // Jakákoliv změna profiltrování potravin
    @FXML
    private void handleFilter () {
        L.debug("Search: " + search.getText());

        aktualni_zdrave = zdrave.isSelected();
        aktualni_vlastni = vlastni.isSelected();
        hledane = search.getText();

        db.fillTable(tab_menu, tab_potraviny, new Label[] {kalorie_snidane, kalorie_dopol_svacina, kalorie_obed, kalorie_odpol_svacina, kalorie_vecere});
    }
    
    
    // Drag and drop ----------------
    
    
    // FIXME - chová se blbě při úpravě hlavičky tabulky
    @FXML
    private void handleDragEvent(MouseEvent e) {

        TableView t = (TableView) e.getSource();

        if (!t.getSelectionModel().isEmpty() && !aktualni_menu.isEmpty()) {

            Potravina_DB p = (Potravina_DB) t.getSelectionModel().getSelectedItem();

            L.debug("ID drag: " + p.getId());

            if (p.getId() != null) {

                Dragboard d = t.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString(p.getId());
                d.setContent(content);
                e.consume();
            }
        }
    }

    @FXML
    private void handleDragOver(DragEvent e) {

        // data is dragged over the target 
        Dragboard d = e.getDragboard();
        if (d.hasString()) {
            e.acceptTransferModes(TransferMode.COPY);
        }
        e.consume();
    }

    @FXML
    private void handleDropEvent(DragEvent e) {

        Dragboard d = e.getDragboard();
        boolean success = false;
        if (d.hasString()) {

            TableView t = (TableView) e.getSource();
            
            db.copyToTable(DB.TAB_POTRAVINY, DB.TAB_MENU, d.getString());
            
            if (nazev_menu.getText().length() > 0) {
                
                db.updateParam(DB.SL.MENU, nazev_menu.getText());
                db.updateParam(DB.SL.JIDLO, t.getId());
            }
            
            aktualizujTabulky(db, t, tab_menu);
            success = true;

        }
        e.setDropCompleted(success);
        e.consume();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
  
        aktualni_menu = "";
        aktualni_zdrave = zdrave.isSelected();
        aktualni_vlastni = vlastni.isSelected();
        hledane = "";

        db = DB.getDB();
        aktualizujTabulky(db, tab_potraviny, tab_menu);
        
        
        
    }
    
    
    private void aktualizujTabulky(DB db, TableView ... t) {
        
        for (TableView ta : t) {
            db.fillTable(tab_menu, ta, new Label[] {kalorie_snidane, kalorie_dopol_svacina, kalorie_obed, kalorie_odpol_svacina, kalorie_vecere});
        }
        
        db.fillEatsKcal(new Label[] {kalorie_snidane, kalorie_dopol_svacina, kalorie_obed, kalorie_odpol_svacina, kalorie_vecere});
        
    }
    
}
