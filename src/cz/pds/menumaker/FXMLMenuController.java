/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.menumaker;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author honya
 */
public class FXMLMenuController implements Initializable {

    @FXML
    private TextField tuky;
    @FXML
    private TextField bilkoviny;
    @FXML
    private TextField vaha_bilkoviny;
    @FXML
    private TextField sacharidy;
    @FXML
    private TextField tolerance;
    @FXML
    private TextField snidane;
    @FXML
    private TextField dopol_svacina;
    @FXML
    private TextField obed;
    @FXML
    private TextField odpol_svacina;
    @FXML
    private TextField vecere;
    @FXML
    private Label verze;

    DB db;
    
    @FXML
    private void handleOk(ActionEvent e) {

        db.cleanTable(DB.TAB_CILE);
        
        Map<String, Object> m = new HashMap<>();
        
        m.put(DB.SL.TUKY.toString(), tuky.getText());
        m.put(DB.SL.BILKOVINY.toString(), bilkoviny.getText());
        m.put(DB.SL.PC_BILKOVINY.toString(), vaha_bilkoviny.getText());
        m.put(DB.SL.SACHARIDY.toString(), sacharidy.getText());
        m.put(DB.SL.TOLERANCE.toString(), tolerance.getText());
        
        m.put(FXMLDocumentController.TABLES.tab_menu_snidane.toString(), snidane.getText());
        m.put(FXMLDocumentController.TABLES.tab_menu_dopol_svacina.toString(), dopol_svacina.getText());
        m.put(FXMLDocumentController.TABLES.tab_menu_obed.toString(), obed.getText());
        m.put(FXMLDocumentController.TABLES.tab_menu_odpol_svacina.toString(), odpol_svacina.getText());
        m.put(FXMLDocumentController.TABLES.tab_menu_vecere.toString(), vecere.getText());
    
        db.insertTarget(DB.TAB_CILE, m);
        
        // Které tlačítko jsem zmáčknul
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.close();

    }
    
    
    @FXML
    private void handleStorno(ActionEvent e) {
        
        // Které tlačítko jsem zmáčknul
        Button b = (Button) e.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        stage.close();
        
    }
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        db = DB.getDB();
        
        tuky.setText(db.queryValue("SELECT " + DB.SL.TUKY + " FROM " + DB.TAB_CILE + ";"));
        bilkoviny.setText(db.queryValue("SELECT " + DB.SL.BILKOVINY + " FROM " + DB.TAB_CILE + ";"));
        vaha_bilkoviny.setText(db.queryValue("SELECT " + DB.SL.PC_BILKOVINY + " FROM " + DB.TAB_CILE + ";"));
        sacharidy.setText(db.queryValue("SELECT " + DB.SL.SACHARIDY + " FROM " + DB.TAB_CILE + ";"));
        tolerance.setText(db.queryValue("SELECT " + DB.SL.TOLERANCE + " FROM " + DB.TAB_CILE + ";"));
        
        snidane.setText(db.queryValue("SELECT " + FXMLDocumentController.TABLES.tab_menu_snidane + " FROM " + DB.TAB_CILE + ";"));
        dopol_svacina.setText(db.queryValue("SELECT " + FXMLDocumentController.TABLES.tab_menu_dopol_svacina + " FROM " + DB.TAB_CILE + ";"));
        obed.setText(db.queryValue("SELECT " + FXMLDocumentController.TABLES.tab_menu_obed + " FROM " + DB.TAB_CILE + ";"));
        odpol_svacina.setText(db.queryValue("SELECT " + FXMLDocumentController.TABLES.tab_menu_odpol_svacina + " FROM " + DB.TAB_CILE + ";"));
        vecere.setText(db.queryValue("SELECT " + FXMLDocumentController.TABLES.tab_menu_vecere + " FROM " + DB.TAB_CILE + ";"));
        
        Package p = getClass().getPackage();
        verze.setText("Verze: " + p.getImplementationVersion());
        
    }
    
}
