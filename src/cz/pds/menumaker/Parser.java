/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.pds.menumaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Parser {

    private BufferedReader b;
    private DB db;
    
    enum HTML {
        
        H1("<h2>", "</h2>"),
        H2("<h3>","</h3>"),
        P("<p>","</p>"),
        BR("<br>","<br>"),
        NL("\n","\n");
        
        private String open, close;
        
        HTML(String o, String c) {open = o; close = c;}
        String getOpen() { return open; }
        String getClose() { return close; }
    }
    
    

    public Parser(BufferedReader b) {
        this.b = b;
        db = DB.getDB();
    }

    public static double parseDouble(String s) throws ParseException {
        String text;
        
        if (s != null) {
            text = s.replace(",", ".");
            text = text.replace(" ", "");
        } else {
            text = "0";
        }

        return Double.parseDouble(text);
    }
    
    public static String formatPercent(String s) {

        double d;

        try {
            d = parseDouble(s);
        } catch (Exception e) {
            d = 0;
        }
                 
        return String.format("%.1f", d);
    }
    
    public static String formatInt (String s) {
        
        double d;

        try {
            d = parseDouble(s);
        } catch (Exception e) {
            d = 0;
        }

        return String.format("%,d", (int) d);
    }
    
    public void parseCSV() throws IOException {

        String line;
        Map<String, Object> m = new HashMap<>(10);

        // Parse head (first line)
        String[] h = b.readLine().split(";");

        for (int i = 0; i < h.length; i++) {

            switch (h[i]) {
                case "Zdravé potraviny":
                    h[i] = DB.SL.ZDRAVE.toString();
                    break;
                case "Název":
                    h[i] = DB.SL.NAZEV.toString();
                    break;
                case "Bílkoviny":
                    h[i] = DB.SL.BILKOVINY.toString();
                    break;
                case "Sacharidy":
                    h[i] = DB.SL.SACHARIDY.toString();
                    break;
                case "Tuky":
                    h[i] = DB.SL.TUKY.toString();
                    break;
                case "Energie":
                    h[i] = DB.SL.ENERGIE.toString();
                    break;
                default:
                    h[i] = "";
            }
        }

        // For every line
        while ((line = b.readLine()) != null) {
            
            // Because of numbers
            line = line.replace(",", ".");
            // Parse CSV line
            String[] l = line.split(";");

            for (int i = 0; i < l.length; i++) {          
                m.put(h[i], l[i]);
            }
            
            // Importované nejsou vlastní = 0
            m.put(DB.SL.VLASTNI.toString(), 0);
            
            db.insert(DB.TAB_POTRAVINY, m);
            m.clear();
            
        }
    }
    
    public static String[] menuToHtml (ArrayList<Potravina_DB> a) {
        
        StringBuilder outHtml = new StringBuilder();
        StringBuilder outText = new StringBuilder();
        
        // main title
        outText.append(a.get(0).getMenu());
        outText.append(System.lineSeparator());
        outText.append(System.lineSeparator());
        
        outHtml.append(HTML.H1.getOpen());
        outHtml.append(a.get(0).getMenu());
        outHtml.append(HTML.H1.getClose());
        
        
        for (FXMLDocumentController.TABLES t : FXMLDocumentController.TABLES.values()) {

            outText.append(t.getLabel());
            outText.append(System.lineSeparator());
            
            outHtml.append(HTML.H2.getOpen());
            outHtml.append(t.getLabel());
            outHtml.append(HTML.H2.getClose());
            outHtml.append(HTML.P.getOpen());
            
            for (Potravina_DB p : a) {

                if (t.toString().equals(p.getJidlo())) {
                    
                    outText.append(p.getVaha());
                    outText.append(" g - ");
                    outText.append(p.getNazev());
                    outText.append(System.lineSeparator());
                    
                    outHtml.append(p.getVaha());
                    outHtml.append(" g - ");
                    outHtml.append(p.getNazev());
                    outHtml.append(HTML.BR.getOpen());

                }
            }
            
            outText.append(System.lineSeparator());
            
            outHtml.append(HTML.P.getClose());
        }
        
        return new String[] {outText.toString(), outHtml.toString()};
    }
    
    
}
